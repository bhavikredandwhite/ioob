# iOOB



## Getting started
Extension for set/get data from array w/o iOOB

## Codes

![CodePic](/CodeImage.png)

- [ ] Extension
```
extension Collection {
    @inlinable func isValid(_ position: Self.Index) -> Bool {
        indices.contains(position)
    }
    @inlinable subscript(safe position: Self.Index) -> Self.Element? {
        isValid(position) ? self[position] : nil
    }
}

extension MutableCollection {
    @inlinable subscript(x position: Self.Index) -> Self.Element? {
        get {
            isValid(position) ? self[position] :  nil
        } set {
            if isValid(position), let newValue = newValue {
                self[position] = newValue
            }
        }
    }
}
```



- [ ] Example
```
var m = [[55.0,66,77,88,99], [33]] 
print("getRow:\t\t\t", m[x: 0] ?? "***", m[x: 5] ?? "***")


m[x: 0]?[x: 0] = 444
m[x: 1]?[x: 8] = 555
print("\nupdateItem:\t\t", m)


m[x: 1] = [666, 777]
m[x: 3] = [888]
print("\nupdateRow:\t\t", m)


var w = [true, false, false, true]
w[x: 1]?.toggle()
print("\nupdateItem:\t\t", w)
w[x: 8]?.toggle()
print("\nupdateItem:\t\t", w)
```

- [ ] Output
```
getRow:			 Optional([55.0, 66.0, 77.0, 88.0, 99.0]) nil

updateItem:		 [[444.0, 66.0, 77.0, 88.0, 99.0], [33.0]]

updateRow:		 [[444.0, 66.0, 77.0, 88.0, 99.0], [666.0, 777.0]]

updateItem:		 [true, true, false, true]

updateItem:		 [true, true, false, true]
```

## License
Open source projects.

## Project status
...
